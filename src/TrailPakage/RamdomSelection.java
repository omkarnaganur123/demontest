package TrailPakage;

import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;

public class RamdomSelection {

	public static void main(String[] args) {
		// random string of length 8 composed of alphabetic characters 
		String Alphabetic = RandomStringUtils.randomAlphabetic(8); 
		System.out.println(Alphabetic);

		// random string of length 8 composed of alphabetic characters and numbers
		String alphanumiric = RandomStringUtils.randomAlphanumeric(8); 
		System.out.println(alphanumiric);

		// random string of length 8 composed only of lettes a, b, and c
		String alphabet = "abc";
		String s = RandomStringUtils.random(8, alphabet);
		System.out.println(s);

		//Now this uuid enter to your text box
		String uuid = UUID.randomUUID().toString();
		System.out.println(uuid);

		
	}

}
